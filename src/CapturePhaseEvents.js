import React from 'react'

export default function CapturePhaseEvents() {
    return (
        <>
            <h2>capture phase events</h2>
            <div onClickCapture={() => {
                /* this runs first */
                alert('hola');
            }}>
                <button onClick={e => {
                    e.stopPropagation();
                    alert('1');
                }} children="1" />
                <button onClick={e => {
                    e.stopPropagation();
                    alert('2');
                }} children={"2"} />
            </div>
        </>
    );
}
