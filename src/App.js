import React from 'react'
import ImpureComp from './ImpureComp'
import FixedImpureComp from './FixedImpureComp'
import EventPropaagtion from './EventPropaagtion'
import StopEventPropagation from './StopEventPropagation'
import CapturePhaseEvents from './CapturePhaseEvents'
import EventDefaultBehavior from './EventDefaultBehavior'
import PreventEventDefaultBehavior from './PreventEventDefaultBehavior'
import AssignVariable from './State/AssignVariable'
import UpdateState from './State/UpdateState'
import MultipleStates from './State/MultipleStates'
import SingleState from './State/SingleState'
import MyStateComp from './State/MyStateComp'
export default function App() {
  return (
    <>
    <ImpureComp/>
    <FixedImpureComp/>
    <EventPropaagtion/>
    <StopEventPropagation/>
    <CapturePhaseEvents/>
    <EventDefaultBehavior/>
    <PreventEventDefaultBehavior/>
    <AssignVariable/>
    <UpdateState/>
    <MultipleStates/>
    <SingleState/>
    <MyStateComp/>
    </>
  )
}

