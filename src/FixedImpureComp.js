import React from 'react'
function Cup({ guest }) {
    return <p>Tea cup for guest #{guest}</p>;
  }
export default function FixedImpureComp() {
  return (
    <>
    <h2>fixed</h2>
      <Cup guest={1} />
      <Cup guest={2} />
      <Cup guest={3} />
    </>
  );
}
