import React from 'react'

export default function PreventEventDefaultBehavior() {
    return (
        <>
        <h2>prevent event default behavior(click button will not reload the whole page)</h2>
            <form onSubmit={e => {
            e.preventDefault();
            alert('Submitting!');
        }}>
            <input />
            <button>Send</button>
        </form>
        </>
    );
}
