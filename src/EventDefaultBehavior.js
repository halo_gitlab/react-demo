import React from 'react'

export default function EventDefaultBehavior() {
  return (
    <>
      {/* a <form> submit event, which happens when a button inside of it is clicked, 
      will reload the whole page by default */}
      <h2>event default behavior(click button will reload the whole page by default)</h2>
      <form onSubmit={() => alert('Submitting!')}>
        <input />
        <button>Send</button>
      </form>
    </>
  );
}
