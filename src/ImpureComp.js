import React from 'react';
let guest = 0;

function Cup() {
    // Bad: changing a preexisting variable!
    //   This component is reading and writing a guest variable declared outside of it.
    //  This means that calling this component multiple times will produce different JSX! 
    // And what’s more, if other components read guest, they will produce different JSX, too,
    //  depending on when they were rendered! That’s not predictable.
    guest = guest + 1;
    return <p>Tea cup for guest #{guest}</p>;
}

export default function ImpureComp() {
    return (
        <>
            <h2>impure component</h2>
            <Cup />
            <Cup />
            <Cup />
        </>
    );
}
