import React from 'react'

export default function EventPropaagtion() {
    return (
        <>
        <h2>event propagation</h2>
            <div className="Toolbar" onClick={() => {
                alert('You clicked on the toolbar!');
            }}>
                <button onClick={() => alert('Playing!')}>
                    Play Movie
                </button>
                <button onClick={() => alert('Uploading!')}>
                    Upload Image
                </button>
            </div>
        </>
    );
}
