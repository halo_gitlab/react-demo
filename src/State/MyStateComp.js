import React ,{useState} from 'react'
import miniUseState from './miniUseState'
// export render function of this component
let render;
export default function MyStateComp() {
    const [a,setA]=miniUseState(0);
    // for re rendering
    const [r,setR]=useState(null);
    render=setR;
    console.dir(setR)
    console.log(a)
    function handle(){
        setA(a+1);
    }
  return (
    <>
        <h2>use miniSetState</h2>
        <div>a:{a}</div>
        {/* for re rendering */}
        {r}
        <button onClick={handle}>a+1</button>
    </>
  )
}
export {render}