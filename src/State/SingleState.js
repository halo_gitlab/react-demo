import React, { useState } from 'react'

export default function SingleState() {
    const [{ a, b }, setState] = useState({ a: 0, b: 1 });
    function handle(flag) {
        if (!flag)
            setState(prevState=>{ return {a: prevState.a + 1, b:prevState.b} })
        else
            setState(prevState=>{ return {a:prevState.a, b: prevState.b + 2 }})
    }
    return (
        <>
            <h2>single state variable</h2>
            <div>a:{a}</div>
            <div>b:{b}</div>
            <input type="button" name="" id="" value="a+1" onClick={()=>handle(0)} />
            <br />
            <input type="button" value="b+2" onClick={()=>handle(1)} />
        </>
    )
}
