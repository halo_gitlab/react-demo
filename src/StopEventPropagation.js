import React from 'react'
function Button({ onClick, children }) {
    return (
        <button onClick={e => {
            e.stopPropagation();
            onClick();
        }}>
            {children}
        </button>
    );
}
export default function StopEventPropagation() {
    return (
        <>
        <h2>stop event propagation</h2>
            <div className="Toolbar" onClick={() => {
                alert('You clicked on the toolbar!');
            }}>
                <Button onClick={() => alert('Playing!')}>
                    Play Movie
                </Button>
                <Button onClick={() => alert('Uploading!')}>
                    Upload Image
                </Button>
            </div>
        </>
    );
}
